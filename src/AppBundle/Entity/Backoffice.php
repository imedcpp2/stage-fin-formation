<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Backoffice
 *
 * @ORM\Table(name="backoffice")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BackofficeRepository")
 */
class Backoffice
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="theme0", type="string", length=255, nullable=true)
     */
    private $theme0;

    /**
     * @var string
     *
     * @ORM\Column(name="theme1", type="string", length=255, nullable=true)
     */
    private $theme1;

    /**
     * @var string
     *
     * @ORM\Column(name="theme2", type="string", length=255, nullable=true)
     */
    private $theme2;

    /**
     * @var string
     *
     * @ORM\Column(name="theme3", type="string", length=255, nullable=true)
     */
    private $theme3;

    /**
     * @var string
     *
     * @ORM\Column(name="theme4", type="string", length=255, nullable=true)
     */
    private $theme4;

    /**
     * @var string
     *
     * @ORM\Column(name="theme5", type="string", length=255, nullable=true)
     */
    private $theme5;

    /**
     * @var string
     *
     * @ORM\Column(name="discription", type="text")
     */
    private $discription;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=100)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="mail", type="string", length=100)
     */
    private $mail;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255)
     */
    private $adresse;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set theme0
     *
     * @param string $theme0
     *
     * @return Backoffice
     */
    public function setTheme0($theme0)
    {
        $this->theme0 = $theme0;

        return $this;
    }

    /**
     * Get theme0
     *
     * @return string
     */
    public function getTheme0()
    {
        return $this->theme0;
    }

    /**
     * Set theme1
     *
     * @param string $theme1
     *
     * @return Backoffice
     */
    public function setTheme1($theme1)
    {
        $this->theme1 = $theme1;

        return $this;
    }

    /**
     * Get theme1
     *
     * @return string
     */
    public function getTheme1()
    {
        return $this->theme1;
    }

    /**
     * Set theme2
     *
     * @param string $theme2
     *
     * @return Backoffice
     */
    public function setTheme2($theme2)
    {
        $this->theme2 = $theme2;

        return $this;
    }

    /**
     * Get theme2
     *
     * @return string
     */
    public function getTheme2()
    {
        return $this->theme2;
    }

    /**
     * Set theme3
     *
     * @param string $theme3
     *
     * @return Backoffice
     */
    public function setTheme3($theme3)
    {
        $this->theme3 = $theme3;

        return $this;
    }

    /**
     * Get theme3
     *
     * @return string
     */
    public function getTheme3()
    {
        return $this->theme3;
    }

    /**
     * Set theme4
     *
     * @param string $theme4
     *
     * @return Backoffice
     */
    public function setTheme4($theme4)
    {
        $this->theme4 = $theme4;

        return $this;
    }

    /**
     * Get theme4
     *
     * @return string
     */
    public function getTheme4()
    {
        return $this->theme4;
    }

    /**
     * Set theme5
     *
     * @param string $theme5
     *
     * @return Backoffice
     */
    public function setTheme5($theme5)
    {
        $this->theme5 = $theme5;

        return $this;
    }

    /**
     * Get theme5
     *
     * @return string
     */
    public function getTheme5()
    {
        return $this->theme5;
    }

    /**
     * Set discription
     *
     * @param string $discription
     *
     * @return Backoffice
     */
    public function setDiscription($discription)
    {
        $this->discription = $discription;

        return $this;
    }

    /**
     * Get discription
     *
     * @return string
     */
    public function getDiscription()
    {
        return $this->discription;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return Backoffice
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set mail
     *
     * @param string $mail
     *
     * @return Backoffice
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return Backoffice
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }
}

