-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 06 juin 2018 à 15:39
-- Version du serveur :  5.7.21
-- Version de PHP :  7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `sagacitelink`
--

-- --------------------------------------------------------

--
-- Structure de la table `backoffice`
--

DROP TABLE IF EXISTS `backoffice`;
CREATE TABLE IF NOT EXISTS `backoffice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `theme0` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `theme1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `theme2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `theme3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `theme4` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `theme5` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `discription` longtext COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mail` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `adresse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `backoffice`
--

INSERT INTO `backoffice` (`id`, `theme0`, `theme1`, `theme2`, `theme3`, `theme4`, `theme5`, `discription`, `telephone`, `mail`, `adresse`) VALUES
(1, 'Mathématiques', 'Biologie', 'Physique Chimie', 'Sciences industrielles', 'Lettres', 'Informatique', 'Parmi les leaders de l\'édition numérique, s’appuyant sur un savoir-faire dans la distribution du livre et dans la librairieuniversitaire, Sagacitelink.com® a pour vocation d\'offrir aux étudiants, enseignants et chercheurs, ainsi qu\'aux professionnels, les meilleures publications Scientifiques et Techniques provenant d’éminents universitaires algériens.\r\n \r\nA télécharger en un simple clic, ces documents pédagogiques, couvrant l\'ensemble des disciplines universitaires, sont à votre disposition.\r\nLettres-Philosophie-Sciences humaines\r\nLangues\r\nClasses préparatoires\r\nMathématiques\r\nPhysique, Chimie et sciences connexes\r\nTechnologie-Sciences de l\'ingénieur\r\nSciences médicales\r\nSciences de la terre et de la vie\r\nSciences économiques et sociales\r\nArchitecture-Urbanisme-Arts\r\nFormation Continue\r\n \r\n \r\nEditions Al-Djazair® -  Librairie Al-djazair®\r\n3, Rue Mohamed Yacef,  16000  Alger -  Algérie\r\nTél : 05 60 57 49 52', '05 60 57 49 52', 'sagacitelink@yahoo.fr', 'Editions Al-Djazair® -  Librairie Al-djazair®3, Rue Mohamed Yacef,  16000  Alger -  Algérie');

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

DROP TABLE IF EXISTS `contact`;
CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8_unicode_ci NOT NULL,
  `sujet` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `contact`
--

INSERT INTO `contact` (`id`, `nom`, `mail`, `message`, `sujet`) VALUES
(1, 'RAFIK BENGRID', 'rbengrid@hotmail.com', 'aaaaaaaaaaaaaaaaa', 'aaaaaaa'),
(2, 'Rafik Bengrid', 'rbengrid@hotmail.com', 'xxxxxxxxxxxxxxxxhuhuyhuhbnuhnnhjin', 'xxxxx'),
(3, 'berngrid', 'imed_cpp2@yahoo.fr', 'dsqdqsdqsdsqdqsdsqdqsdqsdsqdsqdsqdqsdsqdqsddsqdqsdqsdqsdqsdqsd', 'sdsqd'),
(4, 'berngrid', 'imed_cpp2@yahoo.fr', 'Bonjour Monsieur,\r\ndépéche toit ya plus de temps, SAHA FTOUREK\r\ncordialement Mr IMED.', 'Mail de Teset'),
(5, 'khorchani', 'imed_cpp2@yahoo.fr', 'Salem, Monsieur\r\ndepéche toi ya pas beaucoup de temp, et SAHA Ftourek\r\nCordialement imed', 'Demande de teste'),
(6, 'Imed', 'imed_cpp2@yahoo.fr', 'gsfdgkjsdfgkshfgksgsgfshdgfsdgfdsjgfsdjgfsdhfsdfsdfsd', 'Teset'),
(7, 'Imed', 'imed_cpp2@yahoo.fr', 'dfdsfdffffffffffffffffffffffffffffffffffffffffffffffkhfkl,n,njogkn,nkn', 'Demande de teste'),
(8, 'Imed', 'imed_cpp2@yahoo.fr', 'dfdsfdffffffffffffffffffffffffffffffffffffffffffffffkhfkl,n,njogkn,nkn', 'Demande de teste'),
(9, 'Rafique', 'imed_cpp245@yahoo.frg', 'GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGffffffffffffffffffffffffffffffffffffffff', 'GGGGG'),
(10, 'toto', 'imed_cpp2@yahoo.fr', 'ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', 'dfdfdfdf'),
(11, 'aaa', 'imed_cpp2@yahoo.fr', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'aaaaaaaaaaaaaaaaaaaaaaa'),
(12, 'rrrr', 'imed_cpp2@yahoo.fr', 'crgtgvgvtvgtgvtvgtvgtgvcrtvg(fv(fv(fvb(tyby-bvy-nhyèunuèbnbuèu,nub,,kbik,i,ik,iybyhy-byhbbhyybh', 'ddddddd'),
(13, 'berngrid', 'imed_cpp2@yahoo.fr', 'sdxcfcsdcdsvdfvfvfcgvgfbhgb gvhgnhgvnhjnhjnhj', 'dddd');

-- --------------------------------------------------------

--
-- Structure de la table `livres`
--

DROP TABLE IF EXISTS `livres`;
CREATE TABLE IF NOT EXISTS `livres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auteur` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `editeur` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `EAN` double NOT NULL,
  `url_photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nb_pages` int(11) NOT NULL,
  `date` date NOT NULL,
  `theme` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sous_theme` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nouveaute` tinyint(1) NOT NULL,
  `url_piece_jointe` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `livres`
--

INSERT INTO `livres` (`id`, `titre`, `auteur`, `description`, `editeur`, `EAN`, `url_photo`, `nb_pages`, `date`, `theme`, `sous_theme`, `nouveaute`, `url_piece_jointe`) VALUES
(1, 'Fonction entières', 'BELLAID', 'La Programmation Linéaire est un domaine pluridisciplinaire, reconnu non seulement comme une branche de l\'optimisation mathématique mais aussi comme un proche allié des sciences de la décision. L\'étude de ce module requiert autant de connaissances théoriques que de savoir -faire pratique.\r\nCe document repose sur des éléments de cours, dispensé dans le cadre de l\'enseignement du module de Programmation Linéaire .Il est complété par des sujets d\'étude, développés pour couvrir l\'ensemble du programme. Ce manuel est scindé en quatre parties, organisées comme suit :\r\nLa première partie est consacrée à la Théorie de la Programmation Linéaire. Elle est axée sur\r\nla présentation des structures particulières d\'un programme linéaire, l\'exploration des propriétés géométriques et algébriques des solutions, ainsi que l\'exposé de la méthode graphique. Cette phase est suivie du concept de la convexité puis la formulation des théorèmes fondamentaux de la Programmation Linéaire.\r\nLa deuxième partie est dédiée à la méthode du Simplexe. Elle est basée sur les principaux aspects de cet algorithme dont, le test d\'optimalité, la caractérisation de l\'absence d\'une solution optimale ˝nie ainsi que l\'existence de solution optimale multiple. Les cas particuliers de programmes artificiels sont également traités dans ce volet.\r\nLa troisième partie est articulée autour de la notion de Dualité. Elle s\'appuie sur les règles de la dualisation, le théorème des écarts complémentaires exprimant la relation entre deux programmes en dualité, enfin, l\'analyse de sensibilité, permettant de poser la condition de stabilité de la solution optimale si l\'un au moins des paramètres est soumis à un changement.\r\nLa dernière partie regroupe des sujets de synthèse, élaborés pour approfondir les notions décrites dans les précédentes parties.', 'Editions Al-Djazair', 123456789, 'uploads\\photos\\Couv1.png', 1, '2018-05-01', 'Mathematiques', 'analyse', 1, 'd267c48d740591152ddb3a7ccbeba10f.pdf'),
(2, 'Sujets de révision', 'ABDELI', 'La Programmation Linéaire est un domaine pluridisciplinaire, reconnu non seulement comme une branche de l\'optimisation mathématique mais aussi comme un proche allié des sciences de la décision. L\'étude de ce module requiert autant de connaissances théoriques que de savoir -faire pratique.\r\nCe document repose sur des éléments de cours, dispensé dans le cadre de l\'enseignement du module de Programmation Linéaire .Il est complété par des sujets d\'étude, développés pour couvrir l\'ensemble du programme. Ce manuel est scindé en quatre parties, organisées comme suit :\r\nLa première partie est consacrée à la Théorie de la Programmation Linéaire. Elle est axée sur\r\nla présentation des structures particulières d\'un programme linéaire, l\'exploration des propriétés géométriques et algébriques des solutions, ainsi que l\'exposé de la méthode graphique. Cette phase est suivie du concept de la convexité puis la formulation des théorèmes fondamentaux de la Programmation Linéaire.\r\nLa deuxième partie est dédiée à la méthode du Simplexe. Elle est basée sur les principaux aspects de cet algorithme dont, le test d\'optimalité, la caractérisation de l\'absence d\'une solution optimale ˝nie ainsi que l\'existence de solution optimale multiple. Les cas particuliers de programmes artificiels sont également traités dans ce volet.\r\nLa troisième partie est articulée autour de la notion de Dualité. Elle s\'appuie sur les règles de la dualisation, le théorème des écarts complémentaires exprimant la relation entre deux programmes en dualité, enfin, l\'analyse de sensibilité, permettant de poser la condition de stabilité de la solution optimale si l\'un au moins des paramètres est soumis à un changement.\r\nLa dernière partie regroupe des sujets de synthèse, élaborés pour approfondir les notions décrites dans les précédentes parties.', 'Editions Al-Djazair', 123456789, 'uploads\\photos\\couv2.png', 2, '2018-05-02', 'Mathematiques', 'algèbre', 1, 'd267c48d740591152ddb3a7ccbeba10f.pdf'),
(4, 'Topographie', 'HALLLOUZ', 'La Programmation Linéaire est un domaine pluridisciplinaire, reconnu non seulement comme une branche de l\'optimisation mathématique mais aussi comme un proche allié des sciences de la décision. L\'étude de ce module requiert autant de connaissances théoriques que de savoir -faire pratique.\r\nCe document repose sur des éléments de cours, dispensé dans le cadre de l\'enseignement du module de Programmation Linéaire .Il est complété par des sujets d\'étude, développés pour couvrir l\'ensemble du programme. Ce manuel est scindé en quatre parties, organisées comme suit :\r\nLa première partie est consacrée à la Théorie de la Programmation Linéaire. Elle est axée sur\r\nla présentation des structures particulières d\'un programme linéaire, l\'exploration des propriétés géométriques et algébriques des solutions, ainsi que l\'exposé de la méthode graphique. Cette phase est suivie du concept de la convexité puis la formulation des théorèmes fondamentaux de la Programmation Linéaire.\r\nLa deuxième partie est dédiée à la méthode du Simplexe. Elle est basée sur les principaux aspects de cet algorithme dont, le test d\'optimalité, la caractérisation de l\'absence d\'une solution optimale ˝nie ainsi que l\'existence de solution optimale multiple. Les cas particuliers de programmes artificiels sont également traités dans ce volet.\r\nLa troisième partie est articulée autour de la notion de Dualité. Elle s\'appuie sur les règles de la dualisation, le théorème des écarts complémentaires exprimant la relation entre deux programmes en dualité, enfin, l\'analyse de sensibilité, permettant de poser la condition de stabilité de la solution optimale si l\'un au moins des paramètres est soumis à un changement.\r\nLa dernière partie regroupe des sujets de synthèse, élaborés pour approfondir les notions décrites dans les précédentes parties.', 'Editions Al-Djazair', 123456789, 'public\\images\\Couv4.png', 4, '2018-05-04', 'Mathematiques', 'algèbre', 1, 'd267c48d740591152ddb3a7ccbeba10f.pdf'),
(7, 'Analyse numérique', 'KOUCHE', 'La Programmation Linéaire est un domaine pluridisciplinaire, reconnu non seulement comme une branche de l\'optimisation mathématique mais aussi comme un proche allié des sciences de la décision. L\'étude de ce module requiert autant de connaissances théoriques que de savoir -faire pratique.\r\nCe document repose sur des éléments de cours, dispensé dans le cadre de l\'enseignement du module de Programmation Linéaire .Il est complété par des sujets d\'étude, développés pour couvrir l\'ensemble du programme. Ce manuel est scindé en quatre parties, organisées comme suit :\r\nLa première partie est consacrée à la Théorie de la Programmation Linéaire. Elle est axée sur\r\nla présentation des structures particulières d\'un programme linéaire, l\'exploration des propriétés géométriques et algébriques des solutions, ainsi que l\'exposé de la méthode graphique. Cette phase est suivie du concept de la convexité puis la formulation des théorèmes fondamentaux de la Programmation Linéaire.\r\nLa deuxième partie est dédiée à la méthode du Simplexe. Elle est basée sur les principaux aspects de cet algorithme dont, le test d\'optimalité, la caractérisation de l\'absence d\'une solution optimale ˝nie ainsi que l\'existence de solution optimale multiple. Les cas particuliers de programmes artificiels sont également traités dans ce volet.\r\nLa troisième partie est articulée autour de la notion de Dualité. Elle s\'appuie sur les règles de la dualisation, le théorème des écarts complémentaires exprimant la relation entre deux programmes en dualité, enfin, l\'analyse de sensibilité, permettant de poser la condition de stabilité de la solution optimale si l\'un au moins des paramètres est soumis à un changement.\r\nLa dernière partie regroupe des sujets de synthèse, élaborés pour approfondir les notions décrites dans les précédentes parties.', 'Editions Al-Djazair', 123456789, 'public\\images\\couv7.png', 7, '2018-05-07', 'Mathematiques', 'algèbre', 1, 'd267c48d740591152ddb3a7ccbeba10f.pdf'),
(8, 'Analyse Complexe', 'AIDAOUI', 'La Programmation Linéaire est un domaine pluridisciplinaire, reconnu non seulement comme une branche de l\'optimisation mathématique mais aussi comme un proche allié des sciences de la décision. L\'étude de ce module requiert autant de connaissances théoriques que de savoir -faire pratique.\r\nCe document repose sur des éléments de cours, dispensé dans le cadre de l\'enseignement du module de Programmation Linéaire .Il est complété par des sujets d\'étude, développés pour couvrir l\'ensemble du programme. Ce manuel est scindé en quatre parties, organisées comme suit :\r\nLa première partie est consacrée à la Théorie de la Programmation Linéaire. Elle est axée sur\r\nla présentation des structures particulières d\'un programme linéaire, l\'exploration des propriétés géométriques et algébriques des solutions, ainsi que l\'exposé de la méthode graphique. Cette phase est suivie du concept de la convexité puis la formulation des théorèmes fondamentaux de la Programmation Linéaire.\r\nLa deuxième partie est dédiée à la méthode du Simplexe. Elle est basée sur les principaux aspects de cet algorithme dont, le test d\'optimalité, la caractérisation de l\'absence d\'une solution optimale ˝nie ainsi que l\'existence de solution optimale multiple. Les cas particuliers de programmes artificiels sont également traités dans ce volet.\r\nLa troisième partie est articulée autour de la notion de Dualité. Elle s\'appuie sur les règles de la dualisation, le théorème des écarts complémentaires exprimant la relation entre deux programmes en dualité, enfin, l\'analyse de sensibilité, permettant de poser la condition de stabilité de la solution optimale si l\'un au moins des paramètres est soumis à un changement.\r\nLa dernière partie regroupe des sujets de synthèse, élaborés pour approfondir les notions décrites dans les précédentes parties.', 'Editions Al-Djazair', 123456789, 'public\\images\\couv8.png', 8, '2018-05-08', 'Mathematiques', 'algèbre', 1, 'd267c48d740591152ddb3a7ccbeba10f.pdf'),
(9, 'Eléments de cristalographie', 'ALLEG', 'La Programmation Linéaire est un domaine pluridisciplinaire, reconnu non seulement comme une branche de l\'optimisation mathématique mais aussi comme un proche allié des sciences de la décision. L\'étude de ce module requiert autant de connaissances théoriques que de savoir -faire pratique.\r\nCe document repose sur des éléments de cours, dispensé dans le cadre de l\'enseignement du module de Programmation Linéaire .Il est complété par des sujets d\'étude, développés pour couvrir l\'ensemble du programme. Ce manuel est scindé en quatre parties, organisées comme suit :\r\nLa première partie est consacrée à la Théorie de la Programmation Linéaire. Elle est axée sur\r\nla présentation des structures particulières d\'un programme linéaire, l\'exploration des propriétés géométriques et algébriques des solutions, ainsi que l\'exposé de la méthode graphique. Cette phase est suivie du concept de la convexité puis la formulation des théorèmes fondamentaux de la Programmation Linéaire.\r\nLa deuxième partie est dédiée à la méthode du Simplexe. Elle est basée sur les principaux aspects de cet algorithme dont, le test d\'optimalité, la caractérisation de l\'absence d\'une solution optimale ˝nie ainsi que l\'existence de solution optimale multiple. Les cas particuliers de programmes artificiels sont également traités dans ce volet.\r\nLa troisième partie est articulée autour de la notion de Dualité. Elle s\'appuie sur les règles de la dualisation, le théorème des écarts complémentaires exprimant la relation entre deux programmes en dualité, enfin, l\'analyse de sensibilité, permettant de poser la condition de stabilité de la solution optimale si l\'un au moins des paramètres est soumis à un changement.\r\nLa dernière partie regroupe des sujets de synthèse, élaborés pour approfondir les notions décrites dans les précédentes parties.', 'Editions Al-Djazair', 123456789, 'public\\images\\couv9.png', 9, '2018-05-09', 'Mathematiques', 'materiaux', 1, 'd267c48d740591152ddb3a7ccbeba10f.pdf'),
(10, 'Algèbre aléatoire', 'DAHMANI', 'La Programmation Linéaire est un domaine pluridisciplinaire, reconnu non seulement comme une branche de l\'optimisation mathématique mais aussi comme un proche allié des sciences de la décision. L\'étude de ce module requiert autant de connaissances théoriques que de savoir -faire pratique.\r\nCe document repose sur des éléments de cours, dispensé dans le cadre de l\'enseignement du module de Programmation Linéaire .Il est complété par des sujets d\'étude, développés pour couvrir l\'ensemble du programme. Ce manuel est scindé en quatre parties, organisées comme suit :\r\nLa première partie est consacrée à la Théorie de la Programmation Linéaire. Elle est axée sur\r\nla présentation des structures particulières d\'un programme linéaire, l\'exploration des propriétés géométriques et algébriques des solutions, ainsi que l\'exposé de la méthode graphique. Cette phase est suivie du concept de la convexité puis la formulation des théorèmes fondamentaux de la Programmation Linéaire.\r\nLa deuxième partie est dédiée à la méthode du Simplexe. Elle est basée sur les principaux aspects de cet algorithme dont, le test d\'optimalité, la caractérisation de l\'absence d\'une solution optimale ˝nie ainsi que l\'existence de solution optimale multiple. Les cas particuliers de programmes artificiels sont également traités dans ce volet.\r\nLa troisième partie est articulée autour de la notion de Dualité. Elle s\'appuie sur les règles de la dualisation, le théorème des écarts complémentaires exprimant la relation entre deux programmes en dualité, enfin, l\'analyse de sensibilité, permettant de poser la condition de stabilité de la solution optimale si l\'un au moins des paramètres est soumis à un changement.\r\nLa dernière partie regroupe des sujets de synthèse, élaborés pour approfondir les notions décrites dans les précédentes parties.', 'editions Al-Djazair', 123456789, 'public\\images\\Couv12.png', 9, '2018-05-09', 'mathematiques', 'algèbre', 1, 'd267c48d740591152ddb3a7ccbeba10f.pdf'),
(12, 'Mécanique des fluides', 'Allaoua GUERZIZ', 'Se dit d\'un corps dont les molécules ont peu d\'adhésion et peuvent glisser librement les unes sur les autres (liquides) ou se déplacer indépendamment les unes des autres (gaz), de façon que le corps prenne la forme du vase qui le contient.\r\nSe dit d\'un corps liquide à la température ordinaire.', 'Editions Al-Djazair', 123456789, 'uploads\\photos\\couv12.png', 12, '2018-05-12', 'Sciences Industrielles', 'hydraulique', 1, 'd267c48d740591152ddb3a7ccbeba10f.pdf'),
(13, 'Technique de génie urbain', 'Tahar BAOUINI', '', 'Editions Al-Djazair', 123456789, 'public\\images\\couv13.jpg', 13, '2018-05-13', 'Sciences Industrielles', 'urbanisme', 1, 'd267c48d740591152ddb3a7ccbeba10f.pdf'),
(14, 'Cristallographie', 'BELGHICHE Robia', 'La cristallographie est la science des cristaux. Elle étudie la formation, la croissance, la forme extérieure, la structure interne et les propriétés physiques de la matière cristallisée.\r\nLa matière se présente dans la nature sous trois états : solide, liquide et gazeux, les atomes s’arrangent dans la matière de façon plus au moins ordonnés, cet ordre diminue de l’état solide à l’état gazeux.\r\n', 'Editions Al-Djazair', 123456789, 'public\\images\\couv14.jpg', 14, '2018-05-14', 'Sciences Industrielles', 'hydraulique', 0, 'd267c48d740591152ddb3a7ccbeba10f.pdf'),
(15, 'Fonte et acier', 'Said BENSSADA', '2. CLASSIFICATION  DES  ACIERS  ET  DES  FONTES\r\n2.1. Classification des aciers\r\n2.2. Aciers à outils\r\n2.3. Aciers et alliages spéciaux\r\n2.4. Classification des fontes\r\n3. DESIGNATION  NORMALISEE  DES  ACIERS  ET  DES  FONTES\r\n3.1. Désignation des aciers\r\n3.2.  Désignation normalisée des fontes \r\n', 'Editions Al-Djazair', 123456789, 'public\\images\\couv15.jpg', 15, '2018-05-15', 'Sciences Industrielles', 'genie métallurgique', 1, 'd267c48d740591152ddb3a7ccbeba10f.pdf'),
(16, 'Régulation des gènes', 'AMMEUR', 'Sommaire\r\nAvant-propos\r\nIntroduction\r\nLa régulation de l’expression des gènes (Opéron lactose)\r\n1. Qu’est-ce-qu’un opéron ?\r\n2. Le métabolisme du lactose\r\n3. L’opéron lactose\r\n4. Régulation négative de la transcription\r\n5. Les différents allèles des gènes de l’opéron lactose\r\nRéférence Bibliographique\r\n ', 'Editions Al-Djazair', 12345678, 'public\\images\\couv16.jpg', 16, '2018-05-16', 'Biologie', 'génétique', 1, 'd267c48d740591152ddb3a7ccbeba10f.pdf'),
(17, 'Mutations Chromosomiques', 'AMMEUR 2', 'Sommaire\r\nAvant propos\r\nIntroduction\r\nMutations chromosomiques\r\nI.       Les anomalies de nombre\r\nII.      Les anomalies de structure\r\nRéférence Bibliographique', 'Editions Al-Djazair', 12345678, 'public\\images\\couv17.jpg', 17, '2018-05-17', 'Biologie', 'génétique', 1, 'd267c48d740591152ddb3a7ccbeba10f.pdf'),
(18, 'Biosynthèse des proteines', 'AMMEUR 3', 'Sommaire\r\nAvant-propos\r\nIntroduction\r\nBiosynthèse des protéines\r\n1. Structure générale d’un gène\r\n2. Mécanisme de la transcription\r\n3. La traduction\r\n4- Code génétique\r\nRéférence Bibliographique', 'Editions Al-Djazair', 123456, 'public\\images\\couv18.jpg', 18, '2018-05-18', 'Biologie', 'génétique', 0, 'd267c48d740591152ddb3a7ccbeba10f.pdf'),
(19, 'Les glucides', 'MENACEUR', 'Avant propos\r\nVue la grande disponibilité des ressources pédagogiques en Biologie, l’étudiant de biologie peut se retrouver submergé d’informations provenant de différents documents, ce qui pourrait induire une confusion chez lui. La série : Utile en Biologie a pour but d’apporter aux étudiants de 2ème année LMD, les éléments suffisants mais aussi nécessaires de cours des matières essentielles : Biochimie, génétique, Immunologie, Ecologie, Techniques de communications en Anglais….ect. Nous avons évité intentionnellement de détailler certains chapitres qui pourront paraître compliqués ou impliquer d’autres domaines de la biologie.\r\n \r\nNous présentant le cours de Biochimie structurale sous forme de livrent couvrant chacun un groupes de molécules biologiques (Glucides, lipides, protéines, acides nucléiques, enzymes). ', 'Editions Al-Djazair', 123456789, 'public\\images\\couv19.jpg', 19, '2018-05-19', 'Biologie', 'génétique', 0, 'd267c48d740591152ddb3a7ccbeba10f.pdf'),
(20, 'Systèmes à microprocesseur', 'KADRI', 'SOMMAIRE\r\nPARTIE I                CIRCUITS LOGIQUES SEQUENTIELS´\r\nPARTIE II               SYSTEMES A MICROPROCESSEURS`\r\nPARTIE III              MICROPROCESSEUR 8 BITS\r\nPARTIE IV             EXERCICES', 'Editions Al-Djazair', 123456789, 'public\\images\\couv20.jpg', 20, '2018-05-20', 'Informatique', 'microprocesseur', 0, 'd267c48d740591152ddb3a7ccbeba10f.pdf'),
(21, 'Programmation Linéaire', 'ABDELLI-LAHLOU', 'Préface\r\nLa Programmation Linéaire est un domaine pluridisciplinaire, reconnu non seulement comme une branche de l\'optimisation mathématique mais aussi comme un proche allié des sciences de la décision. L\'étude de ce module requiert autant de connaissances théoriques que de savoir -faire pratique.\r\nCe document repose sur des éléments de cours, dispensé dans le cadre de l\'enseignement du module de Programmation Linéaire .Il est complété par des sujets d\'étude, développés pour couvrir l\'ensemble du programme. Ce manuel est scindé en quatre parties, organisées comme suit :\r\nLa première partie est consacrée à la Théorie de la Programmation Linéaire. Elle est axée sur la présentation des structures particulières d\'un programme linéaire, l\'exploration des propriétés géométriques et algébriques des solutions, ainsi que l\'exposé de la méthode graphique. Cette phase est suivie du concept de la convexité puis la formulation des théorèmes fondamentaux de la Programmation Linéaire.\r\nLa deuxième partie est dédiée à la méthode du Simplexe. Elle est basée sur les principaux aspects de cet algorithme dont, le test d\'optimalité, la caractérisation de l\'absence d\'une solution optimale ˝nie ainsi que l\'existence de solution optimale multiple. Les cas particuliers de programmes artificiels sont également traités dans ce volet.\r\nLa troisième partie est articulée autour de la notion de Dualité. Elle s\'appuie sur les règles de la dualisation, le théorème des écarts complémentaires exprimant la relation entre deux programmes en dualité, enfin, l\'analyse de sensibilité, permettant de poser la condition de stabilité de la solution optimale si l\'un au moins des paramètres est soumis à un changement.\r\nLa dernière partie regroupe des sujets de synthèse, élaborés pour approfondir les notions décrites dans les précédentes parties.', 'Editions Al-Djazair', 1233456, 'public\\images\\couv21.jpg', 21, '2018-05-21', 'Informatique', 'programmation', 0, 'd267c48d740591152ddb3a7ccbeba10f.pdf'),
(22, 'Algorithmes d’ajustement', 'KENOUCHE Samir', 'Sommaire\r\n1        Algorithmes d’ajustement\r\n1.1        Régression linéaire\r\n1.2        Régression polynômiale\r\n1.3        Qualité de l’ajustement\r\n1.4        10.3.1 Incertitude sur les paramètres\r\n1.5        Régression non-linéaire\r\n1.5.1         Algorithme de descente de gradient\r\n1.5.2         Algorithme de Gauss-Newton\r\n1.5.3         Algorithme de Levenberg-Marquardt\r\n1.6        Au moyen de routines Matlab ', 'Editions Al-Djazair', 1234, 'public\\images\\couv22.jpg', 22, '2018-05-22', 'Informatique', 'algorithmique', 1, 'd267c48d740591152ddb3a7ccbeba10f.pdf'),
(23, 'Semi-conducteurs', 'AMEUR', 'Table des matières Chapitre I : Notions fondamentales sur la physique des semiconducteurs I.1 Introduction I.2 Notions de bandes énergétiques dans les solides I.2.1 Atome I.3 Semiconducteur I.3.1 Porteurs de charge I.3.2 Dopage I.3.3 Structure en bande d’énergie Chapitre II : Structure cristalline II.1 Introduction II.2 Eléments de cristallographie II.2.1 Réseau périodique d’atomes ou cristal idéal II.2.2 Vecteurs de translation II.2.3 Maille du réseau II.3 Systèmes et réseaux cristallins II.4 Sites dans les réseaux cristallins Chapitre III : Semiconducteur à l’équilibre III.1 Introduction III.2 Différents types de semiconducteurs III.3 Conduction dans les semiconducteurs III.4 Paramètres des semiconducteurs III.4.1 Largeur de la bande interdite (gap) III.4.2 Gap direct et gap indirect III.4.3 Masse effective III.5 Electronique des semiconducteurs intrinsèques III.5.1 Diagramme des bandes III.5.2 Concentration des porteurs de charge III.6 Electronique des semiconducteurs N. III.7 Electronique des semiconducteurs P III.8 Calcul de la position du niveau de Fermi III.8.1 Neutralité électrique Chapitre IV : Semiconducteur hors équilibre IV.1 Introduction IV.2 Phénomène de la Conductivité dans les semiconducteurs IV.3 Densité de courant de conduction IV.4 Courant de diffusion', 'Editions Al-Djazair', 12345678, 'public\\images\\couv23.jpg', 23, '2018-05-23', 'math', 'microprocesseurs', 1, 'd267c48d740591152ddb3a7ccbeba10f.pdf'),
(24, 'Contrôle de gestion', 'OUAMEUR AHMED Abdelmadjid', 'TABLE DES MATIERS\r\n1             INTRODUCTION AU CONTRÔLE DE GESTION\r\n2             DÉFINITIONS DU CONTRÔLE DE GESTION\r\n3             LES PRINCIPAUX OUTILS DU CONTRÔLE DE GESTION\r\n4             LA CESSION INTERNE\r\n5             LA GESTION BUDGÉTAIRE\r\n6             LA MISE EN PLACE DU CONTRÔLE DE GESTION\r\n7             LES OUTILS PRATIQUES DU CONTRÔLE DE GESTION\r\n8             LES MISSIONS DU CONTROLEUR DE GESTION\r\n9             LES AUDITS INTERNES ET EXTERNES\r\n10           CONCLUSION GÉNÉRALE\r\n11           EXERCICES', 'Editions Al-Djazair', 12345678, 'public\\images\\couv24.jpg', 24, '2018-05-24', 'economie', 'gestion', 0, 'd267c48d740591152ddb3a7ccbeba10f.pdf'),
(25, 'Commerce international', 'CHAIB', 'Sommaire\r\nIntroduction\r\nI. Evolution du Commerce international depuis la chute des régimes centralisés\r\nII. Les Turbulences de l’économie Internationale\r\nIII. Les défis de la croissance de l’économie mondiale\r\nIV. Le commerce international de la zone Euro\r\nV. Le commerce international : entre un avenir radieux pour les économies développées et lueur d’espoir pour les économies émergentes\r\nConclusion\r\nBibliographie', 'Editions Al-Djazair', 1236789, 'public\\images\\couv25.jpg', 25, '2018-05-25', 'economie', 'commerce', 1, 'd267c48d740591152ddb3a7ccbeba10f.pdf'),
(26, 'MARKETING MANAGEMENT', 'Ahmed Ouamar Madjid', 'SOMMAIRE\r\n1. QU’EST-CE QUE LE MARKETING\r\n2. LE MARCHE\r\n3. LA COLLECTE DE DONNÉES\r\n4. ÉVALUATION DES SEGMENTS DE MARCHE\r\n5. LE MARKETING MIX OU OPÉRATIONNEL\r\n6. LE PRODUIT\r\n7. MARKETING ET PRIX OU PRICE\r\n8. LE MARKETING ET LA DISTRIBUTION\r\n9. MARKETING ET COMMUNICATION', 'Editions Al-Djazair', 12345, 'public\\images\\couv26.png', 26, '2018-05-26', 'economie', 'marketing', 0, 'd267c48d740591152ddb3a7ccbeba10f.pdf'),
(27, 'Bréviaire de l\'économiste', 'Boumendjel Saïd', 'Abaissement : Il s’agit d’un mot qui exprime la baisse, la chute, l’effondrement ou le fléchissement de ce qui est en relation avec l’activité économique en général. Ainsi, il est souvent question de fléchissement des prix, des recettes ou du chiffre d’affaires. On parle également d’effondrement des cours à la Bourse.\r\nAbattement : Il s’agit d’une déduction faite  sur une somme à payer. En droit fiscal, lit-on sur le Larousse (GDEL, 1982), la technique de l’abattement permet  de moduler la pression fiscale selon les catégories de contribuables. Elle constitue, en ce sens, un complément à la progressivité de l’impôt. L’abattement est dit « à la base » lorsque la matière imposable  est définie à partir d’un plancher qui peut être fixe ou variable. L’abattement est pratiqué aussi bien pour le calcul de l’impôt sur le revenu que pour celui des droits de succession.\r\nAberrance : C’est un mot qui provient du latin « aberrare » qui signifie « s’écarter de ». Ainsi, sur le plan des statistiques, notamment en économie, l’aberrance est la singularité présentée dans une série par une grandeur dont la valeur s’écarte très nettement de la valeur centrale. Elle peut être mise en évidence par l’application d’un multiple des mesures d’écart (écart –type, écart à la médiane).', 'Editions Al-Djazair', 1234567, 'public\\images\\couv27.png', 27, '2018-05-27', 'economie', 'lexique', 0, 'd267c48d740591152ddb3a7ccbeba10f.pdf'),
(32, 'Modèle atomique', 'Salima et Aicha Lakehal', 'Le modèle de Bohr est une théorie physique,  cherchant à comprendre la constitution d\'un atome, et plus particulièrement, celui de l\'hydrogène et des ions hydrogénoïdes (ions ne possédant qu\'un seul électron). Ce modèle repose sur un certain nombre de postulats  que nous allons cités, mais tout d’abord nous essayons de rappeler brièvement les résultats expérimentaux qui furent interprétés par la théorie de Bohr, et qui, par conséquent, contribuèrent le plus à son succès', 'Editions Al-Djazair', 123456789, 'public\\images\\couv102.jpg', 102, '2018-06-02', 'Physique Chimie', 'Chimie moléculaire', 1, 'd267c48d740591152ddb3a7ccbeba10f.pdf'),
(33, 'Halogenes (Les)', 'Chérifa Rabia - Saliha Guermouche', 'SOMMAIRE\r\n L’HYDROGENE\r\nI. Introduction\r\nI.1 Isotopes\r\nI.2 L\'hydrogène solide\r\nII. Caractéristiques\r\nIII. Composés chimiques\r\nIII.1 Hydrures ioniques\r\nIII.2 Composés d’insertion\r\nIII.3 Composés non métalliques\r\nIV. Propriétés acido-basiques et redox\r\nIV. 1 Propriétés acido-basiques\r\nIV.2 Propriétés redox\r\nV. Applications\r\nVI. Production\r\n', 'Editions Al-Djazair', 123456789, 'public\\images\\couv104.jpg', 103, '2018-06-04', 'Physique Chimie', 'Chimie moléculaire', 1, 'd267c48d740591152ddb3a7ccbeba10f.pdf'),
(35, 'Soufre et l\'acide sulfurique ', 'Chérifa Rabia', 'OMMAIRE\r\nLE SOUFRE, L’ACIDE SULFURIQUE\r\nI. Le soufre\r\nI.1 Introduction\r\nI.2 Caractéristiques\r\nI.3 Composés chimiques\r\nI.4 Propriétés acido-basiques et redox\r\nI.4.1 Propriétés acido-basiques\r\nI.4.2 Propriétés redox\r\nI.5 Applications\r\n', 'Editions Al-Djazair', 123456789, 'public\\images\\couv105.jpg', 105, '2018-06-05', 'Physique Chimie', 'Chimie moléculaire', 1, 'd267c48d740591152ddb3a7ccbeba10f.pdf'),
(36, 'Dynamiques des fluides', 'Said Bouabdallah', 'définition d’un fluide réel (visqueux)\r\nLes équations de Navier-Stokes sont des équations difficiles et il s’avère utile de se limiter aux cas simples ou particuliers dont les solutions sont connues. Nous nous limitons aux écoulements incompressibles (pour lesquels .v  0 ) et nous supposons de plus que la viscosité demeure constante. Par ailleurs, les problèmes traités seront tels que les conditions aux limites associées aux frontières sont simples à appliquer mathématiquement. ', 'Editions Al-Djazair', 123456789, 'public\\images\\couv106.png', 106, '2018-05-06', 'Physique chimie', 'materiaux', 1, 'd267c48d740591152ddb3a7ccbeba10f.pdf'),
(37, 'Physique statistique', 'BOUABDALLAH Said', 'Chapitre 1 : Introduction à la physique statistique et notions de probabilité\r\n1.       Introduction.\r\n2.       Statistique classique de Maxwell-Boltzmann.\r\n3.       Etat macroscopique état microscopique.\r\n4.       Statistique classique de Maxwell-Boltzmann.\r\n5.       Répartition la plus probable.\r\n6.       Relation de Boltzmann (Relation entre entropie S et nombre de complexions W).', 'Editions Al-Djazair', 123456789, 'public\\images\\couv107.jpg', 107, '2018-06-07', 'Physique chimie', 'materiaux', 1, 'd267c48d740591152ddb3a7ccbeba10f.pdf'),
(38, 'Cinématique des fluides', 'BOUABDALLAH Said', 'Table de Matière\r\n1. Définition\r\n2. Ecoulements potentiels\r\n3. Equations de base pour un écoulement potentiel de fluide incompressible\r\n4. Ecoulement potentiel bidimensionnel de fluide incompressible\r\n5. Solutions pour des écoulements potentiels bidimensionnels à base de la théorie de la\r\nvariable complexe', 'Editions Al-Djazair', 123456789, 'public\\images\\couv108.png', 108, '2018-06-08', 'Physique chimie', 'materiaux', 1, 'd267c48d740591152ddb3a7ccbeba10f.pdf');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_8D93D649C05FB297` (`confirmation_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`) VALUES
(1, 'admin', 'admin', 'imed_cpp@yahoo.fr', 'imed_cpp@yahoo.fr', 1, NULL, '$2y$13$beO41qAZoY/D5VB5iQaJzOxbHwuHO8BXyLZYoA35VgAHcfsLLcz6e', '2018-06-06 14:30:29', NULL, NULL, 'a:0:{}');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

DROP TABLE IF EXISTS `utilisateurs`;
CREATE TABLE IF NOT EXISTS `utilisateurs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pwd` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_497B315E5126AC48` (`mail`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`id`, `nom`, `prenom`, `mail`, `pwd`) VALUES
(7, 'Bengrid', 'Rafik', 'rbengrid@hotmail.com', 'Azerty123'),
(8, 'Khorchani', 'Imed', 'imed_cpp2@yahoo.fr', 'Azerty123'),
(9, 'BENGRID', 'Aurele', 'rafikbengrid@yahoo.fr', 'Azerty123');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
